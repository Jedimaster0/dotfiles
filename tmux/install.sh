#!/usr/bin/env bash
cwd=$(dirname "$(readlink -f "$0")")
now=$(date '+%Y-%m-%dT%H-%M-%S')
GITDIR="${HOME}/git"

# Colors
RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'


################################
# Tmux configurations
################################
if [ -d ${GITDIR}/oh-my-tmux/.git ]; then
  cd ${GITDIR}/oh-my-tmux
  git pull
  cd "${cwd}"
else
  git clone https://github.com/gpakosz/.tmux.git "${GITDIR}/oh-my-tmux"
fi

if [ -f ${HOME}/.tmux.conf ]; then
  if [ ! -L ${HOME}/.tmux.conf ]; then
    # backkup existing config
    mv -v ${HOME}/.tmux.conf ${HOME}/.tmux.conf_${now}
    echo "Existing .tmux.conf found in user home. Backup created: ${HOME}/.tmux.conf_${now}"
    ln -fs "${GITDIR}/oh-my-tmux"/.tmux.conf ${HOME}/.tmux.conf
  fi
else
  ln -fs "${GITDIR}/oh-my-tmux"/.tmux.conf ${HOME}/.tmux.conf
fi

cp -vn ${cwd}/files/tmux.conf.local ${HOME}/.tmux.conf.local

echo tmux installer complete

