#!/usr/bin/env bash
cwd=$(dirname "$(readlink -f "$0")")
now=$(date '+%Y-%m-%dT%H-%M-%S')
VIMDIR="${HOME}/.vim"
GITDIR="${VIMDIR}/bundle"

# Colors
RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'

mkdir -p ${VIMDIR}/autoload ${VIMDIR}/bundle ${VIMDIR}/colors ${VIMDIR}/plugged

# Install vim-plug plugin manager
# Plugins are managed in ./files/vimrc.plugins
curl -fLo ${VIMDIR}/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Install files
if [ -f ${HOME}/.vimrc ]; then
  if [ ! -L ${HOME}/.vimrc ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vimrc ${VIMDIR}/backups/vimrc_${now}
    echo "Existing .vimrc found in user home. Backup created: ${VIMDIR}/backups/vimrc_${now}"

    ln -fs ${cwd}/files/vimrc ${HOME}/.vimrc
  fi
else
  ln -fs ${cwd}/files/vimrc ${HOME}/.vimrc
fi
echo

if [ -f ${HOME}/.vim/vimrc.config ]; then
  if [ ! -L ${HOME}/.vim/vimrc.config ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vim/vimrc.config ${VIMDIR}/backups/vimrc.config_${now}
    echo "Existing vimrc.config found in ${VIMDIR}. Backup created: ${VIMDIR}/backups/vimrc.config_${now}"

    ln -fs ${cwd}/files/vimrc.config ${HOME}/.vim/vimrc.config
  fi
else
  ln -fs ${cwd}/files/vimrc.config ${HOME}/.vim/vimrc.config
fi
echo

if [ -f ${HOME}/.vim/vimrc.key ]; then
  if [ ! -L ${HOME}/.vim/vimrc.key ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vim/vimrc.key ${VIMDIR}/backups/vimrc.key_${now}
    echo "Existing vimrc.key found in ${VIMDIR}. Backup created: ${VIMDIR}/backups/vimrc.key_${now}"

    ln -fs ${cwd}/files/vimrc.key ${HOME}/.vim/vimrc.key
  fi
else
  ln -fs ${cwd}/files/vimrc.key ${HOME}/.vim/vimrc.key
fi
echo

if [ -f ${HOME}/.vim/vimrc.plugin.config ]; then
  if [ ! -L ${HOME}/.vim/vimrc.plugin.config ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vim/vimrc.plugin.config ${VIMDIR}/backups/vimrc.plugin.config_${now}
    echo "Existing vimrc.plugin.config found in ${VIMDIR}. Backup created: ${VIMDIR}/backups/vimrc.plugin.config_${now}"

    ln -fs ${cwd}/files/vimrc.plugin.config ${HOME}/.vim/vimrc.plugin.config
  fi
else
  ln -fs ${cwd}/files/vimrc.plugin.config ${HOME}/.vim/vimrc.plugin.config
fi
echo

if [ -f ${HOME}/.vim/vimrc.plugin.key ]; then
  if [ ! -L ${HOME}/.vim/vimrc.plugin.key ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vim/vimrc.plugin.key ${VIMDIR}/backups/vimrc.plugin.key_${now}
    echo "Existing vimrc.plugin.key found in ${VIMDIR}. Backup created: ${VIMDIR}/backups/vimrc.plugin.key_${now}"

    ln -fs ${cwd}/files/vimrc.plugin.key ${HOME}/.vim/vimrc.plugin.key
  fi
else
  ln -fs ${cwd}/files/vimrc.plugin.key ${HOME}/.vim/vimrc.plugin.key
fi
echo

if [ -f ${HOME}/.vim/vimrc.plugins ]; then
  if [ ! -L ${HOME}/.vim/vimrc.plugins ]; then
    # backup existing config
    mkdir ${VIMDIR}/backups
    mv -v ${HOME}/.vim/vimrc.plugins ${VIMDIR}/backups/vimrc.plugins${now}
    echo "Existing vimrc.plugins found in ${VIMDIR}. Backup created: ${VIMDIR}/backups/vimrc.plugins_${now}"

    ln -fs ${cwd}/files/vimrc.plugins ${HOME}/.vim/vimrc.plugins
  fi
else
  ln -fs ${cwd}/files/vimrc.plugins ${HOME}/.vim/vimrc.plugins
fi

echo Vim installer complete

