###################
# Initialize
###################
if [[ -r "${HOME}/.zshrc_env" ]]; then
  source "${HOME}/.zshrc_env"
fi

export ZSHDIR="${HOME}/.zsh"
export CACHEDIR="${ZSHDIR}/cache"
export ZGENDIR="${ZSHDIR}/zgen"

if [ ! -d $ZSHDIR ]; then               # create $ZSHDIR if it does not exist
  mkdir -p $ZSHDIR
fi
if [ ! -d $CACHEDIR ]; then             # create $CACHEDIR if it does not exist
  mkdir -p $CACHEDIR
fi

autoload -Uz compinit colors vcs_info    # load completions
compinit -d $CACHEDIR/zcompdump
colors
compinit


###################
# Base
###################
# ensure we have correct locale set
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

setopt notify                     # Report the status of background jobs immediately, rather than waiting until just before printing a prompt.
setopt auto_pushd                 # Make cd push the old directory onto the directory stack.
setopt pushd_ignore_dups          # Don’t push multiple copies of the same directory onto the directory stack.
setopt extended_glob              # treat '#', '~' and '^' characters as part of patterns for filename generation
setopt no_case_glob               # case insensitive matching when performing filename expansion
setopt auto_cd                    # if command not found, but directory found, cd into this directory
setopt no_cdable_vars             # turn off automatic matching of ~/ directories (speeds things up)
setopt multios                    # perform implicit tees or cats when multiple redirections are attempted
setopt no_hup                     # (not in documentation ???) do not send the HUP signal to backround jobs on shell exit
setopt prompt_subst               # parameter expansion, command substitution and arithmetic expansion are performed in prompts
setopt auto_remove_slash          # self explicit
setopt chase_links                # resolve symlinks
#setopt glob_dots                  # include dotfiles in globbing
setopt print_exit_value           # print return value if non-zero
#setopt VI                         # another way to enable VI mode
setopt ZLE                        # Ensure the zsh line editor is enabled (default)
setopt globcomplete               # Enable the expansion of glob via zsh completers or something like that. You won\'t get the glob menu without this
unsetopt beep                     # Disable annoying beep
unsetopt bg_nice                  # no lower prio for background jobs
unsetopt hist_beep                # no bell on error in history
unsetopt hup                      # no hup signal at shell exit
unsetopt list_beep                # no bell on ambiguous completion
#bindkey -v                        # vi mode sucks in zsh, so enabling emacs mode. Use ctrl+x+e to open your command in vim for editing


###################
# History
###################

HISTFILE=${ZSHDIR}/zsh_history    # History file
HISTSIZE=50000                    # History file lines
SAVEHIST=10000                    # Shell History lines
setopt bang_hist                  # !keyword
setopt extended_history           # record timestamp of command in HISTFILE
setopt hist_expire_dups_first     # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups           # ignore duplicated commands history list
setopt hist_ignore_space          # ignore commands that start with space
setopt hist_verify                # show command with history expansion to user before running it
setopt inc_append_history         # add commands to HISTFILE in order of execution
setopt hist_reduce_blanks         # reduce unnecessary blanks from commands being written to history
setopt share_history              # share command history data

# make some commands not show up in history
# http://zsh.sourceforge.net/Doc/Release/Parameters.html
## Really wish this would work...
export HISTORY_IGNORE='..*|-*|history*|clear*|exit*'
zshaddhistory() {
  emulate -L zsh
  ## uncomment if HISTORY_IGNORE
  ## should use EXTENDED_GLOB syntax
  #setopt extendedglob
  [[ "${1}" != ${~HISTORY_IGNORE} ]]
}


###################
# COLORS
###################


if whence dircolors > /dev/null; then
  eval "`dircolors -b`"
  alias ls='ls --color=auto'      # make ls always work with colors
  alias less='less -R'            # make less always work with colored input
  alias watch='watch --color'     # make watch always work with colored input
fi


###################
# Completion
###################


# If a pattern for filename generation has no matches, print an error,
# instead of leaving it unchanged in the argument list. This also
# applies to file expansion of an initial ~ or =.
unsetopt nomatch

setopt hash_list_all              # Hash everything before completion
setopt completealiases            # Complete aliases
setopt list_ambiguous             # Complete as much of a completion until it gets ambiguous


# This causes tab to automatically complete when there is a single result/match
zstyle '*' single-ignored complete

# cache completions
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $CACHEDIR

# auto rehash commands
# http://www.zsh.org/mla/users/2011/msg00531.html
zstyle ':completion:*' rehash true

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
 
# Set to built in ls colors
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Skip list and go strait to the selection menu
zstyle ':completion:*' menu select
zstyle ':completion:*' force-list always

# generate descriptions with magic.
zstyle ':completion:*' auto-description 'specify: %d'

# list of completers to use
zstyle ':completion:*' completer _expand _complete _approximate _ignored

# Make the list prompt friendly
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'

# Make the selection prompt friendly when there are a lot of choices
zstyle ':completion:*:default' select-prompt $'\e[01;35m -- Match %M    %P -- \e[00;00m'

# fuzzy matching for typos
zstyle ':completion:*:approximate:::' original
zstyle ':completion:*:approximate:::' max-errors 2 numeric

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# cd will never select parent
zstyle ':completion:*:cd:*' ignore-parents parent pwd

# insert all expansions for expand completer. Seems to disable the glob menu
#zstyle ':completion:*:expand:*' original
 
# man zshcontrib
#zstyle ':vcs_info:*' actionformats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
#zstyle ':vcs_info:*' formats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
#zstyle ':vcs_info:*' enable git #svn cvs

# tab completion for PIDs
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -ax -o pid,user,comm,command -w -w"
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*' force-list always

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*' group-name ''
zstyle ':completion:*:messages' format $'\e[01;35m -- %d -- \e[00;00m'
zstyle ':completion:*:warnings' format $'\e[01;31m -- No Matches Found -- \e[00;00m'
zstyle ':completion:*:descriptions' format $'\e[01;33m -- %d -- \e[00;00m'
zstyle ':completion:*:corrections' format $'\e[01;33m -- %d -- \e[00;00m'
zstyle ':completion:*:manuals' separate-sections true
 
# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:scp:*' tag-order files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:mosh:*' tag-order users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:mosh:*' group-order users hosts-domain hosts-host hosts-ipaddr

# Not sure what this really does but a lot of people have it... Docuemntation is sketchy
zstyle :compinstall filename "${HOME}/.zshrc"


###################
# zgen / plugin magic
###################

zstyle :omz:plugins:ssh-agent agent-forwarding on

source "${ZGENDIR}/zgen.zsh"

# if the init scipt doesn't exist
if ! zgen saved; then
  # Powerlevel10k prompt theme
  zgen load romkatv/powerlevel10k powerlevel10k
  # Load more completion files for zsh from the zsh-lovers github repo.
  zgen load zsh-users/zsh-completions
  # Add Fish-like autosuggestions to your ZSH.
  zgen load zsh-users/zsh-autosuggestions
  zgen load djui/alias-tips
  zgen load supercrabtree/k
  zgen load micha/resty

  # oh my zsh plugins
  zgen oh-my-zsh
  zgen oh-my-zsh plugins/ssh-agent
  zgen oh-my-zsh plugins/colored-man-pages
  zgen oh-my-zsh plugins/git
  zgen oh-my-zsh plugins/python
  zgen oh-my-zsh plugins/pip
  zgen oh-my-zsh plugins/npm
  zgen oh-my-zsh plugins/jsontools
  zgen oh-my-zsh plugins/encode64
  zgen oh-my-zsh plugins/rsync
  zgen oh-my-zsh plugins/common-aliases
  zgen oh-my-zsh plugins/command-not-found
  zgen oh-my-zsh plugins/history
  zgen oh-my-zsh plugins/systemd
  zgen oh-my-zsh plugins/tmux
  zgen oh-my-zsh plugins/terraform
  zgen oh-my-zsh plugins/helm
  zgen oh-my-zsh plugins/golang
  zgen oh-my-zsh plugins/docker
  zgen oh-my-zsh plugins/vi-mode

  # zgen autoupdater
  zgen load unixorn/autoupdate-zgen

  zgen save
fi


###################
# Custom Plugin Settings
###################

# press ctrl + <space> to accept auto suggestions
bindkey '^ ' autosuggest-accept
bindkey '^w' forward-word

# set autosuggest highlight color to cyan
#export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=6'


###################
# custom scripts
###################

# Test if on wsl and set ssh-agent options
#IS_WSL=`grep -i microsoft /proc/version`
#if [ ! "$IS_WSL" = "" ]; then
#  # ssh-agent
#  if [ -z "$(pgrep ssh-agent)" ]; then
#    rm -rf /tmp/ssh-*
#    eval $(ssh-agent -s) > /dev/null
#  else
#    export SSH_AGENT_PID=$(pgrep ssh-agent)
#    export SSH_AUTH_SOCK=$(find /tmp/ssh-* -name "agent.*")
#  fi
#fi


if [[ -r "/etc/profile.d/motd" ]]; then
  source "/etc/profile.d/motd"
fi
if [[ -r "/etc/profile.d/bash_aliases" ]]; then
  source "/etc/profile.d/bash_aliases"
fi
if [ -d /etc/profile.d ]; then
  for i in /etc/profile.d/*.zsh; do
    if [ -r $i ]; then
      source $i
    fi
  done
  unset i
fi
if [[ -r "${HOME}/.bash_aliases" ]]; then
  source "${HOME}/.bash_aliases"
fi
if [[ -r "${HOME}/.bash_aliases_local" ]]; then
  source "${HOME}/.bash_aliases_local"
fi
if [[ -r "${HOME}/.zshrc_local" ]]; then
  source "${HOME}/.zshrc_local"
fi
if [[ -f "${HOME}/.fzf.zsh" ]]; then
  source "${HOME}/.fzf.zsh"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Must be loaded at bottom
# Load zsh-syntax-highlighting before zsh-history-substring-search
zgen load zsh-users/zsh-syntax-highlighting
zgen load zsh-users/zsh-history-substring-search

